import numpy as np
import pandas
import os
import sys
import numbers
import math
from sklearn.decomposition import pca
import torch
import matplotlib.pyplot as plt
def test00():
    a = np.zeros(shape=(5, 10))
    for i in range(len(a)):
        for j in range(len(a[0])):
            k = (math.sqrt(i) + 1.) * 0.07
            a[i, j] = 0.1 * k + j * 1

    print('The array: ', a)
    return a
def test01(arr):
    solver = pca.PCA(n_components=2)
    principal = solver.fit_transform(arr)
    print('Pincipal components: ', principal, '--')
    return principal
if __name__ == '__main__':
    a = test00()
    principle = test01(a)
    np.set_printoptions(threshold=sys.maxsize)
    b = np.dot(np.transpose(a), principle)
    print(b, '========')
    for j in range(len(b[:, 0:])):
        print('', b[j][0] + 1.58113883e-01)
