import numpy as np
import pandas
import os
import sys
import numbers
import math
from sklearn.decomposition import pca
import torch
import matplotlib.pyplot as plt
import mpld3

def getGeneName(rowNo, df):
    return df.values[rowNo+1:rowNo+2, 3:4][0][0]
def getCellName(colNo, df):
    return df.columns.values[colNo+4]
def MYPCA(data, k=2):
    # preprocess the data
    X = torch.from_numpy(data)
    X_mean = torch.mean(X,0)
    X = X - X_mean.expand_as(X)

    # svd
    U,S,V = torch.svd(torch.t(X))
    return torch.mm(X,U[:,:k])
def getData():
    fn = '/home/oefish/Desktop/wu/wu000.csv'
    df = pandas.read_csv(fn)
    # a = df[2:3]
    # for k, v in a.items():
    #     print(k, '--', v.values[0])
    #
    a0 = df.values[1:, 4:]

    # print(df.columns.values)
    # print(a0[0, 0], 'gene: ', getGeneName(0, df), ', cell', getCellName(0, df))

    # Examble nditer. Also consider nditer([a, b]) where a and b are both arrays.
    # a = np.arange(6).reshape(2, 3)
    # for x in np.nditer(a, op_flags=['readwrite']):
    #     x[...] = 2 * x

    a1 = np.zeros(shape=(len(a0), len(a0[0])))

    for i in range(len(a1)):
        for j in range(len(a1[0])):
            try:
                lv = float(a0[i, j])
                a1[i,j] = 0 if math.isnan(lv) else lv
            except:
                a1[i, j] = 0
                pass
    return df, a1
def nditerExample(a1):
    for x in np.nditer(a1):
        print(x)
        i = i +1
        if i > 100:
            break
def draw(arr, df):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1)
    # fig, ax = plt.subplots()
    ax.set_xlabel('Principal Component 1', fontsize=15)
    ax.set_ylabel('Principal Component 2', fontsize=15)
    ax.set_title('Gene PCA analysis', fontsize=20)
    ax.grid()
    targets = ['cat1', 'cat2', 'cat3']
    colors = ['r', 'g', 'b']

    # for target, color in zip(targets, colors):
    scatter = ax.scatter(arr[:, 0], arr[:, 1], c=colors[1], s=50, cmap=plt.cm.jet)
    # plt.show()

    labels = ['']* len(arr[:, 0])
    for i in range(len(arr[:, 0])):
        labels[i] = str(i) + ' - ' + getGeneName(i, df)
    tooltip = mpld3.plugins.PointLabelTooltip(scatter, labels=labels)
    mpld3.plugins.connect(fig, tooltip)
    mpld3.show()

if __name__ == '__main__':
    df, a1 = getData()
    solver = pca.PCA(n_components=2)
    principal = solver.fit_transform(a1)

    # print('Principal: ', principal[:, 0])
    print('Explain ratio: ', solver.explained_variance_ratio_)
    print('Explain: ', solver.explained_variance_)

    # df1 = pandas.DataFrame(data=principal, columns=['col1', 'col2'])
    # df1.head(100)
    # print(df1)

    draw(principal, df)

    # cellPc = np.dot(np.transpose(a1), principal)
    # np.set_printoptions(threshold=sys.maxsize)
    # cellScore = np.sum(cellPc, axis=1)
    # print(cellScore)